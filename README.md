---
vim: et ts=2 sts=2 sw=2
title: Audit de logiciels libres
keywords: audit oss libre
author:
  - Marc Chantreux <marc.chantreux@renater.fr>
---

# Historique

Rencontres Framasoft / RENATER aux JRES 2017:

* Comment faire une audit "autocentré" : suis-je en capacité de me rendre compte si mon projet est ouvert à la contribution ou pas ?
* Comment faire pour rendre les projets identifiables et générer plus d'utilisation ?
* Projet : single-page ; 100 (?) questions ; réponses "oui/non/en cours" ; si "non", micro-aide pour orienter + "qui contacter ?" + "Quoi lire ?"

# Notes

* Comment éviter la crainte "houla, va falloir gérer ces contributeur⋅ices !" ?
* Comment éviter l'effet "labelisation" ?

# Idées à creuser

* développer les compagnons
* monter une formation "licences" pour les services juridiques (des choses existent: à défricher)
* faire des formations et workshops "libération de code"

# Réticences constatées à la contribution

## marre de payer "pour les autres"

## ouvrir, ça ne marche pas

  cf. [spa](spa.md)

## nous n'avons pas les moyens

## je n'ai pas les compétences/moyens de maintenir un logiciel libre

* passez au moins au niveau 1
* du support devrait être disponible
  * services juridiques connaissent mal la question des licences libres.
    * développer le compagnonnage (l'encourager au niveau des ministères)
    * un "appui" d'une institution (reconnue comme légitime) qui permettrait de
      "donner confiance" (et de légitimer) les choix de licences.
      (le principe de subsidiarité s'applique, la DINSIC n'a pas vocation
      à produire des recommandations légales ou technologiques).
      [vvl](http://vvlibri.org/fr) est une bonne source.
      * page datagouv: licences homologuées ... 
      * [https://entrepreneur-interet-general.etalab.gouv.fr/docs/mini-guide-logiciel-libre.pdf]
        * pourquoi ouvrir son code ?
        * choix licence

  * les développeurs sont peu sensibilisés aux problèmes de versions, forges,
    ...

# liens utiles

# Notes sur le fonctionnement d'un réseau de blue hats

* créer une liste annonces@ pour les ouvertures de codes et releases 
  (idéalement il faut faire l'annonce d'intention et mutualiser les développements ?)
* identification des compagnons

# ressources dispos

EIG: entrepreneurs d’intérêt général

* [EIG](https://github.com/entrepreneur-interet-general/eig-link/blob/master/opensource.org)
* [FAQ EIG](https://github.com/entrepreneur-interet-general/eig-link/blob/master/opensource-faq.org)
* [best practices du code infrastructure](https://bestpractices.coreinfrastructure.org/fr)
* [Write the doc](https://www.writethedocs.org/)

# cas concrets

réfléchir ensemble sur "quoi améliorer" ?

    * https://github.com/DGFiP/Test-Compta-Demat/tree/master/src/testeur
    * http://sympa.org

# licence

This work is licensed under a
[Creative Commons Attribution-ShareAlike 2.0 Generic License](https://creativecommons.org/licenses/by-sa/2.0/)



