---
vim: et ts=2 sts=2 sw=2
title: Audit de logiciels libres
keywords: audit oss libre
date: 4 décembre 2018
author:
  - Marc Chantreux <marc.chantreux@renater.fr>
  - Bastien G. (Etalab)
  - Harmonie V. (DINSIC)
  - Marc C. (Unistra)
  - PY G. (Framasoft)
---

la SPA de validation

Planning
décembre : 
    [marc] on ouvre un dépôt sur git ;  
    [tous] on trouve un nom ;
    [bastien] éventuellement, première alpha, d'ici le 20/12
    [tous] à partir du 20 décembre, on ouvre à la liste
    [bastien] contacter la liste des 12 personnes, pour les inviter à contribuer fin décembre + janvier
    [marc] ouvrir dépôt sur framagit

A partir de Janvier : 
    [frama] mettre un peu de temps de dév pour lisser le site
    [tous] poursuivre 
    
Février :


Questions / Catégories

(réponses sous la forme comment? quoi lire? qui contacter?) 
(questions fermées + "je ne sais pas")


# spread the word
  * conférences
  * communications
  *  readme + doc ( references + tutos )
  * publicité/acces au sources
  * code of conduct

# code quality
* coding guidelines
* QA
* packaging


# communication
  * Le projet dispose-t-il d'un site web ?
  * Le site web (ou le Readme) est-il multilingue ?
  * La page de présentation du projet explique-t-elle clairement comment contribuer ?
  * Les issues sont-elles correctement catégorisées ?
  * Des captures écrans (si adapté) sont-elles disponibles ?
  * Le site web, et le Readme, expliquent-ils clairement et succintement ce que fait le logiciel (quel problème il résout) ?
    * Avez-vous testé la compréhension de ce texte par un public "non averti" ?
 
# communauté
  * Vous sentez-vous prêt à accueillir une communauté de contributeurs ?
  * Y a-t-il une personne en charge de l'accueil de nouveaux contributeurs et de l'animation de la communauté ?
  * Cette personne est-elle une autre personne que le développeur principal ?
  * Y a-t-il une personne en charge du triage des issues ?
  * Cette personne est-elle une autre personne que le développeur principal ?


  
# juridique
* Avez vous les droits sur le code ?
*savez-vous ce qu'est une licence libre ?
*Avez-vous choisi une licence libre pour votre projet ?





